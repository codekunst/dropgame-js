Q.scene("intro",function(stage) {
  stage.insert(new Q.Title({ x: 400, y: 300 }));

  stage.insert(new Q.UI.Text({ 
      label: "PRESS SPACE",
      color: "black",
      align: 'center',
      x: Q.width/2,
      y: Q.height/2 + 100
    }));

  Q.input.on("fire",this,function() {
    Q.stageScene("level");
  }); 
  
});
