Q.scene("game_over",function(stage) {
  stage.insert(new Q.Title({ x: 400, y: 300 }));

  stage.insert(new Q.UI.Text({
      label: "GAME OVER",
      color: "black",
      align: 'center',
      shadow: 10,
      shadowColor: "rgba(0,0,0,0.5)",
      size: 65,
      x: Q.width/2,
      y: Q.height/2 - 60
    }));

  stage.insert(new Q.UI.Text({
      label: "PRESS SPACE FOR NEW GAME",
      color: "black",
      align: 'center',
      x: Q.width/2,
      y: Q.height/2 + 130
    }));

  stage.insert(new Q.UI.Text({
      label: "Score: " + Q.state.get("score"),
      color: "black",
      align: 'center',
      x: Q.width/2,
      y: Q.height/2 + 50
    }));

  Q.input.on("fire",this,function() {
    Q.stageScene("level");
  });

});
