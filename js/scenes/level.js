Q.scene("level",function(stage) {

  Q.state.set("score",0);
  Q.state.set("level",1);
  Q.state.set("lives",3);

  //Q.state.reset({ score: 0, lives: 2 });

  stage.insert(new Q.Background());
  stage.insert(new Q.Player());
  //stage.insert(new Q.Icon());

  stage.insert(new Q.IconDropper());

  var container = stage.insert(new Q.UI.Container({
        fill: "gray",
        border: 4,
        shadow: 10,
        shadowColor: "rgba(0,0,0,0.5)",
        y: 0,
        x: 0
      }));


  stage.insert(new Q.Score(), container);
  stage.insert(new Q.Level(), container);
  stage.insert(new Q.Lives(), container);

  if(Q.inputs['Pause']) {
    stage.pause();
  }

});
