
Q.Sprite.extend("Player",{
    init: function(p) {
      this._super(p, {
          x: 54,
          y: Q.height - 30 ,
          z: 0,
          collisionMask: Q.SPRITE_DEFAULT,
          asset: "player.png"
      });
    },
    step: function(dt) {
      this.p.x = Q.inputs['mouseX'];
    }
});

Q.Sprite.extend("Title",{
    init: function(p) {
      this._super(p, {
          x: 0,
          y: 0,
          asset: "title.png"
      });
    }
});

Q.Sprite.extend("Background",{
    init: function(p) {
      this._super(p, {
          x: Q.width/2,
          y: Q.height/2,
          type: 0, // if you don't set the type to zero the icon will collide with it on each frame.
          asset: "background.png"
      });
    }
});

Q.Sprite.extend("Icon",{

    init: function(p) {
      this._super(p, {
          x: 0,
          y: 0,
          vx: 0,
          vy: 100,
          w: 24,
          h: 24,
          z: 0,
          scoreAdd: 10,
          scoreLoose: -10,
          collisionMask: Q.SPRITE_DEFAULT,
          asset: arrIcons[0],
      });

      var assetId = Math.floor((Math.random() * arrIcons.length - 1 ) + 1);

      this.p.x = Math.floor((Math.random() * Q.width) + 1);
      this.p.asset = arrIcons[assetId];

      this.p.vy *= (Q.state.get("level") + 1);
      this.p.vx *= (Q.state.get("level") + 1);

      this.on("hit",this,"collision");

    },
    collision: function(col) {
      if(col.obj.isA("Player")) {

        Q.state.inc("score",this.p.scoreAdd);
        this.destroy();

      }
    },
    step: function(dt) {

      this.stage.collide(this);

      this.p.y += this.p.vy * dt;
      this.p.x += this.p.vx * dt;

      if(this.p.y >= Q.height - (Q.heigth / 10 * Q.state.get("level") ) && !this.p.addedNewIcon) {
        this.stage.insert(new Q.Icon());
      }

      if(this.p.y > Q.height) {

        Q.state.dec("lives",1);
        this.destroy();

      }

    }
});

Q.GameObject.extend("IconDropper",{
  init: function() {
    this.p = {
      launchDelay: 0, //0.75,
      launchRandom: 1,
      launch: 2 / Q.state.get("level")
    }
  },

  update: function(dt) {
    this.p.launch -= dt;

    if(this.p.launch < 0) {
      this.stage.insert(new Q.Icon());
      this.p.launch = this.p.launchDelay + this.p.launchRandom;// * Math.random();
    }
  }

});
