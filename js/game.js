// Now set up your game (most games will load a separate .js file)
var Q = Quintus()     // Create a new engine instance
        .include("Sprites, Scenes, Input, 2D, Touch, UI") // Load any needed modules
        .setup("quintus_canvas",{
              width:   800,         // Set the default width to 800 pixels
              height:  600,         // Set the default height to 600 pixels
              scaleToFit: false     // Scale the game to fit the screen of the player's device
          })                        // Add a canvas element onto the page
        .controls()                 // Add in default controls (keyboard, buttons)
        .touch();

  //Q.debug = true;
  //Q.debugFill = true;
  Q.input.mouseControls();

  Q.input.keyboardControls({
    P: "Pause"
  });
