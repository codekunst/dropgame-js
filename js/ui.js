

Q.UI.Text.extend("Score",{
    init: function(p) {
      this._super({
        label: "Score: 0",
        align: "left",
        x: 10,
        y: 20,
        weight: "normal",
        size:18
      });
      Q.state.on("change.score",this,"score");
    },
    score: function(score) {

      if ((score % 100) === 0) {
        Q.state.inc("level",1);
      }

      this.p.label = "Score: " + score;
    }
  });

  Q.UI.Text.extend("Level",{
      init: function(p) {
        this._super({
          label: "Level: 1",
          align: "center",
          x: Q.width/2,
          y: 20,
          weight: "normal",
          size:18
        });
        Q.state.on("change.level",this,"level");
      },
      level: function(level) {
        this.p.label = "Level: " + level;
        Q.state.inc("lives",1);
      }
    });

  Q.UI.Text.extend("Lives",{
    init: function(p) {
      this._super({
        label: "Lives: 3",
        align: "left",
        x: Q.width - 100,
        y: 20,
        weight: "normal",
        size:18
      });
      Q.state.on("change.lives",this,"lives");
    },
    lives: function(lives) {
      this.p.label = "Lives: " + lives;

      if(lives === 0) {
        Q.stageScene("game_over");
      }

    }
  });

  Q.UI.Text.extend("Pause",{
    init: function(p) {
      this._super({
        label: "Pause",
        align: "center",
        x: Q.width/2,
        y: Q.heigth/2,
        weight: "bold",
        size:24
      });
    }
  });
